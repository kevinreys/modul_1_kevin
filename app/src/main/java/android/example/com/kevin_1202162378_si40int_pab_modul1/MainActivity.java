package android.example.com.kevin_1202162378_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText TinggiBidang;
    private EditText AlasBidang;
    private TextView HasilBidang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    protected void Hitung(View view){

        TinggiBidang = findViewById(R.id.editText);
        AlasBidang = findViewById(R.id.editText2);
        HasilBidang = findViewById(R.id.textView2);

        Integer BidangT = Integer.parseInt(TinggiBidang.getText().toString());
        Integer BidangA = Integer.parseInt(AlasBidang.getText().toString());
        Integer BidangH = BidangT*BidangA;

        HasilBidang.setText(String.valueOf(BidangH));
    }
}
